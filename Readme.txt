This is my library of basic nasm assembly headers for defines, preproccessing macros, inline or called functions, structs and whatever else I can think up.

genreg.ah:
	A header for defining and undefining register names at assembly-time for more independent functions and macros.
hex.ah:
	A header filled with hex to binary and binary to hex conversion functions using the genreg system for modularity.
dec.ah:
	A header filled with decimal to binary and binary to decimal conversions using my generalized register system for increased modularity.
syscall.ah:
	A header detailing some well used syscalls and options on linux. Makes true native system programming much easier.
size.ah:
	A header enabling marginal assembly-time programming of register size. Enables using one macro label for multiple register sizes.
cstring.ah:
	A header for easy declaration of string constants.
debug.ah:
	A debugging header, it allows easy runtime saving, organization, formatting and printing of registers. Currently capable of x64, xmm, and ymm registers, and does so somewhat slowly.
static.ah:
	A header for declaring constant or static memory structures, like tables.
util.ah:
	A header filled with general use and miscellaneous macros like function declaring, reading and sizing zero terminated strings, reversing byte and nibble order, etc. Mostly I haven't found a good place for these yet and that's why they are here.
mouse.ah:
	A header dealing with basic mouse input.
term.ah:
	A header for programming the virtual terminal in ways similar to the curses library in C.
fb.ah:
	A header for programming the linux framebuffer device.

INSTALL:
 Run build.sh with the argument "install" which looks like in full: "./build.sh install" which will ask for SU permissions
 and will copy all the headers to /usr/include. Then, if you haven't already, do "./build.sh once" to append to your .profile
 "export NASMENV=-i/usr/include" which will tell nasm to look in this, our previously copied-to directory, for header files to
 include. For this environment variable to be recognized by nasm, you must log out and back in.
