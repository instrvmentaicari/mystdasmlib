; test for mouse.ah

width	equ 1920		; framebuffer width
height	equ 1080		; framebuffer height

cwidth	equ 64*4	; cell width in grid
cheight	equ 64		; cell height in grid
gwidth	equ 4		; cell width of grid
gheight	equ 4		; cell height of grid
;	--DEFAULT COLORS--
fgcolor	equ 0x00ffffff ; default foreground and background color
ercolor equ 0x00ff1010
bgcolor	equ 0x00000000
mrkcolor equ 0x00ff0000	; marker color -mark_coord-

;	--KEYBINDS--
;		-: see	-KEY SWITCH-
key_close	equ "c"
key_clear	equ "C"
key_mark	equ "m"
key_save	equ "s"
key_redraw	equ	"r"

%define ipath "bittest.bit" ; testing bitwise pixel data
%define opath "bittest.bit" ; all changed data at -bitwise-

%include "head.ah"
%include "genreg.ah"
%include "syscall.ah"
%include "fb.ah"
%include "term.ah"
%include "cstring.ah"
%include "mouse.ah"


c_fehdr main
; file paths
infile:		db ipath,0
outfile:	db opath,0
infd:	dq	0
; framebuffer path
dfbdevpath fb0
; keyboard data
keyin:	db 0

; for forking threads via the function return stack swap technique
kbdstack:	dq kbdthread	; -kbdthread-
mousestack:	dq mousethread	; -mousethread-	UNIMPLIMENTED
start_thread:	; --start_thread--
	mov eax, sys_clone
	mov edi, clone_vm|clone_io|clone_files|clone_thread|clone_sighand
	xor edx, edx
	xor r10, r10
	xor r8, r8
	xor r9, r9
	syscall
	ret

; save screen update bounds in 4 dwords in a 16 byte register to pextrd and pinsrd.

main:

	; set up testing environment

	get_termios tios

	set_noncannon tios	; molest a termios copy
	set_noecho tios

	set_termios tios	; set as new termios

	; setup fb mmap
	open_wr fb0
	mov r13, rax
	bssmmap_w fbuffer, width*height*4, r13, 0
	
	; setup mouse mmap
	opendevmouse 0

	; read file from disk
	open_r	infile
	mov [infd], rax
	mov edi, eax
	xor eax, eax
	mov rsi, buffer
;	mov edx, width*height*4  
	mov edx, (width*height)/8	;--bitwise--
	syscall
	mov eax, sys_close
	mov rdi, [infd]
	syscall

	; start keyboard thread
	mov rsi,	kbdstack
	call start_thread

	; copy initial buffer to framebuffer to display file
	; cpbuff fbuffer, buffer, width*height

lp:
done_old_clean:
	; save last mouse buffer offset
	mov r11, r12	; save previous mouse state in r11
	mov r9, r15	; save previous mouse row byte offset, which gets set to r15 just before previous draw
	mov r15, r14	; save previous mouse row and column byte offset
	

	; get recent mouse updates
	getdevmousein 0
	xor eax, eax

	cmp r12, 1
	je clean_cursor
	ja clean_erase

done_clean:	

	update_cursor 0, 0, width, height	; updates cursor x,y on screen
	calculate_cursor_index 0, 0, width, height

	; calculate row offset of mouse position on the actual display
	xor eax, eax
	mov ax, [cur_0_y]
	mov ecx, width
	mul rcx
	mov rcx, width*height
	sub rcx, rax
	mov rax, rcx

	; save row location for row, based framebuffer updating
	mov r10, rcx
	shl r10, 2

	; calculate column offset
	xor ecx, ecx
	mov cx, [cur_0_x]
	add rax, rcx
	
	; offset by pixel size (4 bytes)
	shl rax, 2
	mov r14, rax

	; load which mouse buttons are down
	ismousebutton1down 0, rbx
	ismousebutton2down 0, rcx
	or bx, cx
	cmp bx, 1
	je mb1	; draw pencil
	ja mb2	; erase area

	; draw arrow
	lea rbx, [r14+fbuffer]
	xor ecx, ecx
	mov rax, [rbx]
	mov rcx, 0x0000ff000000ff00
	xor rcx, rax
	mov [rbx], rcx
	add rbx, width*4
	mov rcx, 0x0000ff00
	mov eax, [ebx]
	xor ecx, eax
	mov [rbx], ecx

	mov r15, r10	; to save "previous starting update line" on next update cycle

	mov r12, 1	; clean_cursor

done_mb:	; also the return location from mb1 and mb2
	
	;drawbuffer r13, pbuffer, width*height*4

	jmp lp

; nominal exit
nomex:	; --nomex--		ref-by: -kbdthread-

	; clear framebuffer for exit
	mov ecx, ((width*height*4)/32)
	xor eax, eax
	vpxor ymm0, ymm0, ymm0
.lp:	
	vmovdqu	[rax+fbuffer], ymm0
	add eax, 32
	loop .lp
	
	set_cannon tios					; born-again virgin
	set_echo tios
	set_termios tios				

	mov eax, sys_exit_group
	xor edi, edi
	syscall


	; draw single white pixel
mb1:
	; copy pixel to print buffer
	mov rsi, r14	;--bitwise--
	mov rdx, r14
	shr rsi, 5
	shr rdx, 2
	mov eax, 1
	and edx, 0b111
	shlx eax, eax, edx
	or [rsi+buffer], al
	mov eax, fgcolor
	lea rbx, [r14+fbuffer]
	mov [rbx], eax

	mov r15, r10	; gets swapped back at lp label
	mov r12, 0 ; clean_none

	jmp lp
	
mb2:
	;--	get sub-byte and byte offset of start
	mov rbx, r14
	mov rdi, r14
	shr rbx, 5
	shr rdi, 2
	add rbx, buffer

	;--	set up 16 bit buffer eraser
	mov eax, -1
	and edi, 0x7
	shlx eax, eax, edi
	and eax, 0xffff
	mov esi, eax
	not esi

	;--	set up 32 byte framebuffer eraser
	lea rdx, [r14+fbuffer]
	mov ecx, 16
	vpxor ymm0, ymm0, ymm0

	;--	erase 16x16 square
.lp:
	and [rbx], si
	and [rbx+2], ax
	vmovdqu [rdx], ymm0
	vmovdqu [rdx+32], ymm0
	add rbx, width/8
	add rdx, width*4
	loop .lp
	vmovdqu [rdx], ymm0
	vmovdqu [rdx+32], ymm0
	and [rbx], si
	and [rbx+2], ax

	; setup square brush pointer
	lea rbx, [r14+fbuffer]
	mov rax, ercolor | (ercolor << 32);0x0000ff000000ff00

	; draw square brush to pbuffer
	xor [rbx], rax
	xor [rbx+56], rax
	add rbx, width*4
	xor [rbx], eax
	xor [rbx+60], eax
	add rbx, width*4*14
	xor [rbx], eax
	xor [rbx+60], eax
	add rbx, width*4
	xor [rbx], rAx
	xor [rbx+56], rax
	
	mov r15, r10	; save row offset

	mov r12, 2 	; clean_erase
	jmp lp

; clean this frame's bullshit

; for cleaning after cursor point
clean_cursor:	;--clean_cursor--
	mov rbx, r15
	mov r8, rbx
	shr rbx, 5
	shr r8, 2
	mov cx, [rbx+buffer] ; little endian so ch will have index 0 and cl will have index 1
	and r8, 0b111
	shrx ecx, ecx, r8d

	mov rsi, 0x0000000100000001
	movq xmm1, rsi
	pdep rax, rcx, rsi 
	movq xmm0, rax
	vpcmpeqd xmm0, xmm0, xmm1
	movq rax, xmm0
	mov [r15+fbuffer], rax

	mov al, [rbx+(buffer+(width/8))] 
	shrx eax, eax, r8d
	mov edi, -1
	and eax, esi
	cmovnz eax, edi
	mov [r15+(fbuffer+(width*4))], eax

	jmp done_clean

; for cleaning after eraser square
clean_erase:	;--clean_erase--
	xor eax, eax
	mov rbx, r15				; clean ultimate top
	mov [rbx+fbuffer], rax
	mov [rbx+fbuffer+56], rax

	add rbx, width*4			; clean penultimate top
	mov [rbx+fbuffer], eax
	mov [rbx+fbuffer+60], eax

	add rbx, width*4*14			; clean penultimate bottom
	mov [rbx+fbuffer], eax
	mov [rbx+fbuffer+60], eax

	add rbx, width*4			; clean ultimate bottom
	mov [rbx+fbuffer], rax
	mov [rbx+fbuffer+56], rax

	jmp done_clean

; keyboard code -kbdthread-

draw_grid:	;	--draw_grid--		from	-KEY SWITCH-
	; get drawing brushes
	mov rax, -1					; vertical line
	vpcmpeqb ymm0, ymm0, ymm0	; horizontal line

	mov edx, gheight
	mov rbx, [cur_buff_0_a]
 .lp:
 	mov ecx, (cwidth/32)*gwidth ; how many times a avx2 brush needs to draw this horizontal line
 	xor edi, edi
 .lph:	
 	vmovdqu [rdi+rbx+buffer], ymm0
 	vmovdqu [rdi+rbx+fbuffer], ymm0
 	add rdi, 32
 	loop .lph
	mov [rdi+rbx+buffer],	eax
	mov [rdi+rbx+fbuffer],	eax
 
 	mov ecx, cheight
 .lpv:	
 	xor edi, edi
 	add rbx, width*4
 	mov esi, ecx
 	mov ecx, gwidth
 
 .lpv2:	
 	mov [rdi+rbx+buffer],	eax
 	mov [rdi+rbx+fbuffer],	eax
 	add rdi, cwidth
 	loop .lpv2
 	mov [rdi+rbx+buffer],	eax
 	mov [rdi+rbx+fbuffer],	eax
 
 	mov ecx, esi
 	loop .lpv
 
 	add rbx, width*4
 	dec edx
 	cmp	edx, 0
 	jnz .lp
 
 	mov ecx, (cwidth/32)*gwidth ; how many times a avx2 brush needs to draw this horizontal line
 	xor edi, edi
 .llp:
	vmovdqu [rdi+rbx+buffer], ymm0
	vmovdqu [rdi+rbx+fbuffer], ymm0
	add rdi, 32
	loop .llp

	jmp kbdthread	;	-kbdthread-

redraw_fb:	; --redraw_fb--				from	-KEY SWITCH-
	xor eax, eax
	xor ebx, ebx
	mov r8, (width*height)/64	;--bitwise--
	vpxor ymm0, ymm0, ymm0
	mov rdx, 0x0101010101010101	;--bitwise--
.lp:	
	mov rsi, [rax+buffer]		;--bitwise--
	mov ecx, 8
.slp:
	pdep rdi, rsi, rdx		;--bitwise--
	movd  xmm1, edi			;--bitwise--
	shr rsi, 8			;--bitwise--
	shr rdi, 32			;--bitwise--
	movd  xmm2, edi			;--bitwise--
	vpunpcklbw xmm1, xmm1, xmm1
	vpunpcklbw xmm2, xmm2, xmm2
	vpunpcklwd xmm1, xmm1, xmm1
	vpunpcklwd xmm2, xmm2, xmm2
	vperm2i128 ymm1, ymm2, ymm1, 0x02
	vpcmpgtd ymm1, ymm1, ymm0	;--bitwise--
	vpsrld ymm1, ymm1, 8
	vmovdqu [rbx+fbuffer], ymm1
	add rbx, 32
	sub ecx, 1
	jnz .slp
	add rax, 8			;--bitwise--
	sub r8, 1
	jnz .lp
	jmp kbdthread

clear_fb:	; --clear_fb--				from	-KEY SWITCH-
	mov ecx, ((width*height*4)/32)
	xor eax, eax
	vpxor ymm0, ymm0, ymm0
.lp:	
	vmovdqu	[rax+fbuffer], ymm0
	add eax, 32
	loop .lp
	jmp kbdthread

newstrla savemsg, "File saved!"
save_file:	; --save_file--				from	-KEY SWITCH-
	mov eax, sys_open
	mov rdi, outfile
	mov esi, writeonly|create|truncate
	mov edx, 0o6666
	syscall
	mov edi, eax
	mov eax, sys_write
	mov rsi, buffer
;	mov edx, width*height*4
	mov edx, (width*height)/8 ; --bitwise--
	syscall
	write stdout, ssavemsg, lsavemsg
	jmp kbdthread	;	-kbdthread-

mark_coord:		; --mark_coord--		from	-KEY SWITCH-
	; ACTUALLY MARK CO-ORD
	vmovdqu xmm0, [cur_buff_0_x]		; mouse.ah -curlocbuffstruc-
	vmovdqu [cur_buff_marker_x], xmm0
	mov eax, [cur_0_x]			; mouse.ah -curlocstruc-
	mov [cur_marker_x], eax
	vpextrq rbx, xmm0, 1	
	; DRAW COOL MARKER ON SCREEN
	mov eax, mrkcolor			;	-DEFAULT COLORS-
	movd xmm0,	eax
	vpbroadcastd ymm0, xmm0
	vmovdqu [rbx+(fbuffer-16)],	xmm0	; horizontal line
	mov [rbx+(fbuffer-(width*4))], eax	; top pixels
	mov [rbx+(fbuffer-(width*8))], eax	
	mov [rbx+(fbuffer-(width*12))], eax	
	jmp kbdthread						;	-kbdthread-

delete_to_mark:	; --delete_to_mark--	from	-KEY SWITCH-

	; get line offset
	vmovdqu xmm0, [cur_buff_0_x]
	mov ebx, [cur_buff_marker_y] ; get line offset
	vpextrd eax, xmm0, 1
	cmp rax, rbx
	cmovle rdi, rax	; start y offset
	cmovle rdx, rbx	; end y offset
	cmovg rdi, rbx
	cmovg rdx, rax

	mov ebx, [cur_buff_marker_x] ; get line offset
	movd eax, xmm0
	cmp eax, ebx
	cmovle esi, eax	; start x offset
	cmovle ecx, ebx	; end x offset
	cmovg esi, ebx
	cmovg ecx, eax
	sub ecx, esi	; get difference
	shr ecx, 2
	mov r8, rcx	; save for sub loop counter
	xor eax, eax	; erase brush

.mlp:
	mov rcx, r8	; load sub loop counter
	mov ebx, esi
.slp:
	mov [rdi+rbx+buffer],	eax
	mov [rdi+rbx+fbuffer],	eax
	add ebx,	4
	loop	.slp
	add rdi,	width*4
	cmp rdi,	rdx
	jge kbdthread	;	-kbdthread-
	jmp .mlp

kbdthread:	; --kbdthread--	started-by -start_thread-
.lp:
	read stdin, keyin, 1
	mov   al, [keyin]

;	--KEY SWITCH-- 
;		-: see	-KEYBINDS-
	cmp al, key_close
	je nomex			;-nomex-
	cmp al, "C"
	je clear_fb			;-clear_fb-
	cmp al, "r"
	je redraw_fb		;-redraw_fb-
	cmp al, "s"
	je save_file		;-save_file-
	cmp al, "g"
	je draw_grid		;-draw_grid- 
	cmp al, "i"
	je mark_coord		;-mark_coord-
	cmp al, "M"		
	je delete_to_mark	;-delete_to_mark-
	jmp .lp

; what am I thinking? If the user doesn't want to overwrite framebuffer content drawn by the keyboard, he simply should
; learn not to move his mouse while interacting with it.
; Fucks sake, the point is low latency, not intuitive usability. Fuck grandma.

mousethread:

c_ftr

bsb offsetbeg, (width*height)/16
bsb buffer, (width*height)/8
bsb offsetmid, (width*height*4)/2
alignb 4096
bsb fbuffer, (width*height*4)
bsb offsetend, (width*height*4)/2
alignb 4096
bsb ybuffer, (width*height*4)	; yank buffer -yank-
bsd yb_x,	1					; yank buffer x, y and total size
bsd yb_y,	1
bsq yb_t,	1
bsb	inbuffer, 100
curlocstruc marker
curlocbuffstruc marker			;	-mark_coord-		--marked_coord--
termios_struct tios
mouseinbuff 0
curlocstruc 0
alignb 16	; for proper inter thread communication
curlocbuffstruc 0
e_bss
