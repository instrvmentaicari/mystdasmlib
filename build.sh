jobs=8	# Change this to control thread count of build.
maketests () {
 ls tests | grep -F .a | parallel -j$jobs nasm tests/{} -o tests/{.}
 ls tests | grep -F -x .a | parallel -j$jobs -u chmod +x ./tests/{}
}
test () {
 nasm tests/test$1.a  -o tests/test$1
 chmod +x tests/test$1
 ./tests/test$1
}
#maketests	# Assume you want to make the tests at least if you run the build
install(){
 chmod 644 *.ah
 sudo cp ./*.ah /usr/include/
}
once(){
 echo export NASMENV=-i/usr/include/ >>~/.profile
}
#tools for configuring primary input devices or system at large.
tools(){
 ls tools | grep -F .a | parallel -j$jobs nasm tools/{} -o tools/{.}
 ls tools | grep -F .a | parallel -j$jobs chmod +x tools/{.}
}
$1 $2
