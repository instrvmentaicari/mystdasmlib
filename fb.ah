; library containing macros pertaining to linux frame buffer manipulaition.
%define libfb 1
%ifndef genregs
	%error Need genreg.ah included above fb.ah.
%endif

%macro dfbpath 1
	; path to fb device
	%defstr %%str %1
	%[%1]: db "/dev/",%%str,0x0
	%define singlefb 1
%endmacro
; memory map a static fb into bss data
%macro setsinglefb 0
 %define singlefb 1
%endmacro
%macro setmultifb 0
 %define singlefb 0
%endmacro
%macro mmapsfb 1
	 %define %%buffer fb
	 %define %%ls fb_line_size
	 %define %%lc fb_line_count
mov r8, %1 ; fd
mov eax, 9 ; mmap
mov rdi, %%buffer
mov rsi, %%ls*%%lc*4
mov rdx, 3 ; prot_read|prot_write
mov r10, 0x8011 ; shared | fixed | populate
xor r9, r9
syscall
%endmacro
; basic stack for simple draw thread management
%macro drawthreadstack 2 ; %1 is target function label, %2 is count of threads
draw_thread_%[%1]_responsibility: dw (fb_line_count/%2) ; Give count of lines each thread draws
%assign %%i 0
%rep %2
; putting the target line directly behind the target function for
draw_target_%[%1]_%[%%i]: dw ((fb_line_count/%2)* %%i) ; Give line target for thread
draw_stack_%[%1]_%[%%i]: dq %1 ; target function
%assign %%i %%i + 1
%endrep
%endmacro
%macro startdrawthreads 3 ; %1 is draw function label, %2 is launching function label, %3 is count of threads
%assign %%i 0
%rep %3
 mov rsi, draw_stack_%[%1]_%[%%i]
 call %2
%assign %%i %%i + 1
%endrep
%endmacro
%macro sqfill 6
	; %1 is x offset %2 is y offset
	; %3 is x size %4 is y size
	; %5 is color
	 %define %%ls fb_line_size
	; calculate line location offset
	mov eax, %2
	mov edi, %1
	shl rax, 2
	shl rdi, 2
	mov ecx, %%ls
	mul rcx
	; calculate x position and thus buffer offset
	lea rdi, [rax+rdi]
	add rdi, %6
	mov rsi, rdi
	; calculate final y pos start location
	mov rax, %%ls
	shl rax, 2
	mov ecx, %4
	mul rcx
	lea rdx, [rax+rdi]
	mov eax, %5
%%loop:
	mov rdi, rsi
	mov ecx, %3
%%sloop:
	mov eax, %5
	stosd
	loop %%sloop
	add rsi, (%%ls*4)
	cmp rsi, rdx
	ja %%exit
	jmp %%loop
%%exit:
%endmacro

; for setting a single pixel in data
%define spx(color) 0xffffffff&color
; for setting 4 double words at a time
%define s4px(first,second,third,fourth) spx(first),spx(second),spx(third),spx(fourth)
%define s8px(fst,scnd,thd,frth,fth,sxth,svth,eith) s4px(fst,scnd,thd,frth),s4px(fth,sxth,svth,eith)

%macro drawbuffer 3
	%define %%fd %1
	%define %%buffer %2
	%define %%res %3
	mov rdx, (%%res)*4
;	shl rdx, 2
	mov rsi, %%buffer
	mov rdi, %%fd
	xor r10, r10
	mov eax, 18
	syscall
%endmacro

%macro copy_pow32_bitmap 5	;	destination - source - destination line width - source width in 32 byte blocks - source height in pixels - ymm register to copy with
%push scope
%pop
%endmacro

; copy buffer
%macro cpbuff 3
	mov rcx, (%3)/32
	mov rax, 0
%%lp:	
	vmovdqu ymm0, [%2+rax]
	vmovdqu [%1+rax], ymm0
	add rax, 32
	loop %%lp
%endmacro

; convert sixteen color byte to raw 0RGB quadword 
%macro sxtncbtorawq 3
%push scope
	dgl inr, %1	; input register,		general register,	scratched
	dgl our, %2	; output register,		general register,	scratched
	dgl tr, %3	; temporary register,	general register,	scratched
	; setup
	mov tr, 0x0001010100010101
	; get base color,
	pdep tr, inr, tr
	mov our, tr
	shl tr, 1
	or our, tr
	shl tr, 2
	or tr, our
	shl tr, 4
	or our, tr

	; set light or dark pixel state
	shr inr, 6
	mov tr, 0x0000008000000080
	pdep tr, ir, tr 
	mov ir, tr
	shl tr, 8
	or  ir, tr
	shl tr, 8
	or  ir, tr
	and our, ir

%pop
%endmacro	

; convert raw 0RGB yword to 16 color dword
%macro rawqwto16cb 6
%push scope
	dgr inr, %2	; input register,		ymm register,		scratched
	dgr our, %1	; output register,		general register,	scratched
	dgr tr0, %3	; temporary register,	general register,	scratched
	dgr tr3, %4	; temporary register,	general register,	scratched
	dgr tr1, %5	; temporary register,	ymm register,		scratched
	dgr tr2, %6	; temporary register,	ymm register,		scratched

	; get bytemask of existing bytes
	mov tr0d, 0x7f
	movd tr2x,	tr0d
	vpbroadcastb tr2y, tr2x
	vpand tr1y, inry, tr2y
	vpxor tr2y, tr2y, tr2y
	vpcmpgtb tr1y, tr1y, tr2y
	vpextrq tr0, yr1x, 0

	mov tr3, 0x01010100010101	; deposit first 6 bits into first byte
	pdep our, tr0, tr3

	vpextrq tr0, yr1x, 1		; deposit second 6 bits into second byte
	pdep tr0, tr0, tr3
	shl tr0, 8					


%pop
%endmacro	
