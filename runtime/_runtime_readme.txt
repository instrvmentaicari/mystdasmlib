Chunks of truely position independent code that is not in a header, does not need to be assembled every time and only needs to (d)included and jumped into.
Each file represents a single function, or function variant, and gets assembled once.
Releases of the library will feature this library fully assembled for you, so you do not need to do so yourself.
Each function follows loosely the C calling abi, but jumps to r15 instead of returning.
This is better because it allows runtime decisionmaking as to where the next tailcall is directed, along with not fucking with the stack.
Sometimes I may write code that does not return on r15 but on r14 or r13 because the nature of the function may imply it being the first in a trio or more of components making up a greater whole.
Because position independent code, utilized in this manner, doesn't know its memory location, you must pass the target function location on rax.

Due to the fact that this is statically linked, I choose a different ABI to interface with this type of PIC.
 __________________________________________________________________________
| Arg1 | Arg2 | Arg3 | Arg4 | Arg5 | Arg6 | FUNC_MEM_LOC | STACK_ARG_COUNT |
| RAX  | RBX  | RCX  | RDX  | RDI  | RSI  | R8           | r9              |
 --------------------------------------------------------------------------
