#a simple script to generate or append to a make file for linkless nasm via head.ah
echo "./$1:	./$1.a"	>> makefile
echo "	nasm ./$1.a -o ./$1" >> makefile
echo "	chmod 755 ./$1" >> makefile
